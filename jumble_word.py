import random
# function for choosing random word.
def choose():
    words = ['rainbow', 'computer', 'science', 'programming','mathematics', 'player', 'condition', 'reverse','water', 'board','hissing','anaesthesia','biology']
    pick = random.choice(words)
    return pick


# Function for shuffling the characters of the chosen word.
def jumble(word):
    random_word = random.sample(word, len(word))
    jumbled = ''.join(random_word)
    return jumbled


# Function for playing the game.
def play():
    player = input("Please enter your name :")
    print("Hi {}!!!".format(player))
    print("LET'S BEGIN GAME>>>>")
    picked_word = choose()
    jumbled_word= jumble(picked_word)
    print("Jumbled word is",jumbled_word)
    print(player.upper(),"What's in your mind?")
    ans = input()
    if ans == picked_word:
        print("Congratulations {}!!!You guessed the correct word which is '{}'".format(player.upper(),picked_word))
    else:
        print("OOPS!!!You guessed the wrong word..!!")
        print("correct word is:",picked_word)
        print("Better luck next time {}:(".format(player.upper()))
play()
print("Thanks for playing...")
